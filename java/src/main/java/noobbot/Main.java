package noobbot;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import noobbot.messages.CreateRace;
import noobbot.messages.Join;
import noobbot.messages.JoinRace;
import noobbot.messages.SendMsg;
import noobbot.messages.Switch;
import noobbot.messages.Throttle;
import noobbot.messages.Turbo;
import noobbot.models.CarPosition;
import noobbot.models.CarPositionData;
import noobbot.models.Crash;
import noobbot.models.Dnf;
import noobbot.models.Finish;
import noobbot.models.MessageType;
import noobbot.models.Race;
import noobbot.models.RaceData;
import noobbot.models.Spawn;
import noobbot.models.Track;
import noobbot.models.TurboAvailable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

public class Main {

    private static String botName;
    private static String botKey;
    private static int port;
    private static String host;
    private static String racename;
    private static String password;
    private static String nbCars;
    private Boolean qualif = true;

    private static enum Command {
        JOIN,
        CREATERACE,
        JOINRACE
    }

    private static Command command;

    public static void main(String... args) throws IOException {
        if (args.length >= 4) {
            host = args[0];
            port = Integer.parseInt(args[1]);
            botName = args[2];
            botKey = args[3];
            if (args.length >= 5) {
                command = Command.valueOf(args[4]);
//                System.out.println("commande : " + command);
                if (args.length >= 6) {
                    racename = args[5];
                    if (args.length >= 7) {
                        nbCars = args[6];
                        if (args.length >= 8) {
                            password = args[7];
                        }
                    }
                }
            } else {
                command = Command.JOIN;
            }
        } else {
            printUsage();
        }

//        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        switch (command) {
            case JOIN:
                new Main(reader, writer, new Join(botName, botKey));
                break;
            case CREATERACE:
                new Main(reader, writer, new CreateRace(botName, botKey, racename, nbCars, password));
                break;
            case JOINRACE:
                new Main(reader, writer, new JoinRace(botName, botKey, racename, nbCars, password));
                break;
            default:
                printUsage();
                break;
        }
    }


    private static void printUsage() {
        System.out.println("Usage : run server port botname botkey [ command [ racename [ password ] ] ]");
        System.out.println("command : JOINRACE / CREATERACE");
        System.out.println("racename : germany, keymola");
    }

    private final Gson gson = new Gson();
    private PrintWriter writer;
    private boolean dataLoaded = false;
    private Race race = null;

    private SwitchCalculator switchCalculator;
    private MBSpeedCalculator speedCalculator;
    private TurboCalculator turboCalculator;
    int bumpCount = 0;

    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join) throws IOException {
        int turboFactor = 1;
        int gameTickTurbo = 0;
        this.writer = writer;
        String line = null;
        TurboAvailable turbo = null;
        float currentSpeed = 0;

        boolean crashed = false;
        List<String> carFinished = Lists.newArrayList();

        send(join);
        while ((line = reader.readLine()) != null) {
            final MessageType messageType = gson.fromJson(line, MessageType.class);
            switch (messageType.msgType) {
                case "carPositions":
                    SendMsg command = null;
                    CarPositionData carPositionsData = getCarPositionsData(line, gson);
                    int gameTick = carPositionsData.gameTick;
                    if (!crashed) {
                        float speedCommand = 1f;
                        if (bumpCount > 0) {
                            bumpCount--;
                        }

                        if (dataLoaded) {
                            CarPosition myCarPosition = getMyCarPosition(carPositionsData);
                            currentSpeed = speedCalculator.getCurrentSpeed(myCarPosition, currentSpeed);
                            int currentPieceIndex = myCarPosition.piecePosition.pieceIndex;
                            Switch.Side switchCommand = switchCalculator.getSwitchCommand(myCarPosition, currentPieceIndex, bumpCount, qualif);
                            if (switchCommand != null && !switchCommand.equals(Switch.Side.NOTHING)) {
                                command = new Switch(switchCommand, gameTick);
                            } else {

                                boolean bump = false;
                                for (CarPosition carPosition : carPositionsData.carPositions) {
                                    String carName = carPosition.id.name;
                                    //On ignore soi même et les voitures qui ont déja été disqualifiées ou ont terminé
                                    if (!carName.equals(botName) && !carFinished.contains(carName)) {
                                        CarPosition.PiecePosition piecePosition = carPosition.piecePosition;
                                        int pieceIndexOpponent = piecePosition.pieceIndex;
                                        boolean sameLane = myCarPosition.piecePosition.lane.endLaneIndex == piecePosition.lane.endLaneIndex;
                                        boolean onMyPiece = pieceIndexOpponent == currentPieceIndex && myCarPosition.piecePosition.inPieceDistance < piecePosition.inPieceDistance;
                                        boolean onNextPiece = pieceIndexOpponent == (currentPieceIndex + 1) % race.nbPieces;

                                        if (sameLane) {
                                            float distanceBetweenCars = getDistanceBetweenCars(myCarPosition, carPosition);
                                            if (distanceBetweenCars > 0 && distanceBetweenCars < 50) {
                                                bump = true;
                                                bumpCount = 10;
//                                                System.out.println("BUMMMMMMMMMMMMMMMMMMMMP " + gameTick);
                                                break;
                                            }
                                        }
                                    }
                                }

                                if (turbo != null && turboCalculator.getTurboCommand(currentPieceIndex)) {
                                    gameTickTurbo = turbo.data.turboDurationTicks;
                                    turboFactor = turbo.data.turboFactor;
                                    turbo = null;
                                    command = new Turbo(gameTick);
//                                } else if (bump) {
//                                    speedCommand = 1f;
                                } else {
                                    speedCommand = speedCalculator.getSpeedCommand(myCarPosition, currentPieceIndex, currentSpeed, gameTickTurbo, turboFactor, bump);
                                }
                            }
                        }

                        if (command == null) {
                            command = new Throttle(speedCommand, gameTick);
                        }
                    } else {
                        command = new Throttle(1f, gameTick);
                    }
                    if (gameTickTurbo > 0) {
                        gameTickTurbo--;
                    }
                    send(command);
                    break;
                case "join":
//                    System.out.println("Joined");
                    break;
                case "gameInit":
//                    System.out.println("Race init");
                    //Obliger de le reload, car le raceSession à changer
                    race = getRaceObject(gson, line, 19f, 5000f);
                    if (!dataLoaded) {
                        Runnable runnable = new Runnable() {
                            @Override
                            public void run() {
                                switchCalculator = new SwitchCalculator(race);
                                speedCalculator = new MBSpeedCalculator(race);
                                turboCalculator = new TurboCalculator(race);
                                dataLoaded = true;
                            }
                        };
                        new Thread(runnable).start();
                    } else {
                        qualif = false;
                        switchCalculator.reset();
                        speedCalculator.reset(race);
                        currentSpeed = 0;
                        gameTickTurbo = 0;
                        turbo = null;
                    }
                    //SI le CI ne build plus c'est à cause de ça
//                    send(new Throttle(1f));
                    break;
                case "gameStart":
//                    System.out.println("Race start");
                    send(new Throttle(1f, 0));
                    break;
                case "turboAvailable":
                    turbo = gson.fromJson(line, TurboAvailable.class);
                    break;
                case "crash":
                    Crash crash = gson.fromJson(line, Crash.class);
                    if (crash.data.name.equals(botName)) {
                        crashed = true;
//                        System.out.println("/!\\ CRASH !!!");
                    }
                    break;
                case "spawn":
                    Spawn spawn = gson.fromJson(line, Spawn.class);
                    if (spawn.data.name.equals(botName)) {
                        crashed = false;
//                        System.out.println("/!\\ SPAWN !!!");
                    }
                    break;
                case "dnf":
                    Dnf dnf = gson.fromJson(line, Dnf.class);
                    carFinished.add(dnf.data.car.name);
                    break;
                case "finish":
                    Finish finish = gson.fromJson(line, Finish.class);
                    carFinished.add(finish.data.name);
                    break;
                case "gameEnd":
//                    System.out.println("Race end");
                    break;
                case "tournamentEnd":
                    return;
                default:
                    break;
            }

        }
    }

    private float getDistanceBetweenCars(CarPosition myCarPosition, CarPosition carPosition) {
        Track.Lane currentLane = race.track.lanes.get(myCarPosition.piecePosition.lane.endLaneIndex);

        int myPieceIndex = myCarPosition.piecePosition.pieceIndex;
        int opponentPieceIndex = carPosition.piecePosition.pieceIndex;

        if (myPieceIndex == opponentPieceIndex) {
            return carPosition.piecePosition.inPieceDistance - myCarPosition.piecePosition.inPieceDistance;
        } else if (myPieceIndex + 1 == opponentPieceIndex) {
            float distOnMyPiece = race.track.pieces.get(myPieceIndex).getDistance(currentLane.distanceFromCenter) - myCarPosition.piecePosition.inPieceDistance;
            float distOnHisPiece = carPosition.piecePosition.inPieceDistance;
            return distOnMyPiece + distOnHisPiece;
        }

        return -1;
    }


    private CarPosition getMyCarPosition(CarPositionData carPositionData) {
        for (CarPosition carPosition : carPositionData.carPositions) {
            if (carPosition.id.name.equals(botName)) {
                return carPosition;
            }
        }

        return null;
    }

    public static Race getRaceObject(Gson gson, String line, Float K, Float masse) {
        RaceData raceData = gson.fromJson(line, RaceData.class);
        Race race = raceData.data.race;
        race.nbPieces = race.track.pieces.size();

        for (int i = 0; i < race.nbPieces; i++) {
            race.track.pieces.get(i).index = i;
        }
        return race;
    }

    public static CarPositionData getCarPositionsData(String line, Gson gson) {
        CarPositionData carPositionData = gson.fromJson(line, CarPositionData.class);

        return carPositionData == null ? null : carPositionData;
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}



