package noobbot;

import com.google.common.collect.Lists;
import noobbot.models.CarPosition;
import noobbot.models.Race;
import noobbot.models.Track;

import java.util.List;

/**
 * User: vmaubert
 * Date: 19/04/14
 * Time: 12:41
 */
public class SpeedCalculator {

    private static float UPDATE_SIZE = 0.5f; // magic number
    public static final int ANGLE_LIMIT = 45; // magic number
    
    private CarPosition previousCarPosition = null;
    private float currentSpeed = 0;
    private float masse = 5000;
    private float K = 19f;
    private final Race race;
    private final Utils utils = new Utils();
    public static float deceleration = -0.0015f;// magic number
    private double distanceRalentissement;
    private double fullDistanceRalentissement =0;
    private float distanceToBend;
    private float kmax=Float.MAX_VALUE;

    public SpeedCalculator(Race race) {
        this.race = race;
    }

    public float getSpeedCommand(CarPosition myCarPosition, int currentPieceIndex, Float currentSpeed) {
        float speedCommand;

        Track.Piece currentPiece = race.track.pieces.get(currentPieceIndex);
        List<Integer> nextBendPieces = getNextBendPieces(currentPieceIndex);
        float nextMinSpeed = getMinSpeed(nextBendPieces);
        double oldDistanceToBend = distanceToBend;
        distanceToBend = getDistance(myCarPosition, nextBendPieces.get(0));

//        System.out.print("Speed " + utils.formatNumber(currentSpeed) + "\tnext min :" + utils.formatNumber(nextMinSpeed) + " \tangle : " + utils.formatNumber(myCarPosition.angle));
        // TODO : regarder à partir d'une certaine distance selon la difficulté du virage
        float force1 = getForce(currentPieceIndex + 1);
        float force2 = getForce(currentPieceIndex + 2);
        float force3 = getForce(currentPieceIndex + 3);
        if (myCarPosition.angle > ANGLE_LIMIT || myCarPosition.angle < -ANGLE_LIMIT) {
            if (K > 1) {
                if (myCarPosition.angle>57 && previousCarPosition.angle <= 57) {
                    kmax = K;
                }
                float kUpdate = UPDATE_SIZE * (Math.abs(myCarPosition.angle) - ANGLE_LIMIT) / 200;
                if (kUpdate < K) {
                    K -= kUpdate;
                }
            }
            if (race.track.pieces.get((currentPieceIndex + 1) % race.nbPieces).isBend()) {
                speedCommand = currentSpeed + ((1 - currentSpeed) / 10);
            } else {
                speedCommand = 1;
            }
        } else {
            if (race.track.pieces.get(currentPieceIndex).isBend()) {
                if (currentSpeed > 0 && (myCarPosition.angle < ANGLE_LIMIT - 10 || myCarPosition.angle > -ANGLE_LIMIT - 10)) {
                    if (K < kmax) {
                        float kUpdate = UPDATE_SIZE * (ANGLE_LIMIT - Math.abs(myCarPosition.angle)) / 1000;
                        K += kUpdate;
                    }
                }
            }


            distanceRalentissement = distanceRalentissement(currentSpeed, nextMinSpeed);
            if (distanceToBend > oldDistanceToBend) {
                fullDistanceRalentissement = distanceRalentissement;
            }

//            System.out.print("\t next bend : " + utils.formatNumber(distanceToBend) + ";" + utils.formatNumber(fullDistanceRalentissement) + " \tral :" + utils.formatNumber(distanceRalentissement * 1.1));
            if (distanceToBend < fullDistanceRalentissement) {
                if (currentSpeed < nextMinSpeed) {
                    speedCommand = nextMinSpeed + ((1 - nextMinSpeed) / 10);
                } else {
                    speedCommand = 0;
                }
            } else {
                if (currentPiece.isBend()) {
                    speedCommand = nextMinSpeed + ((1 - nextMinSpeed) / 10);
                } else {
                    speedCommand = 1;
                }
            }
//            if (force1 > K || force2 > K || force3 > K) {
//                float speedCommand1 = currentSpeed - (currentSpeed - getOptimizedSpeed(currentPieceIndex + 1)) * 4;
//                float speedCommand2 = currentSpeed - (currentSpeed - getOptimizedSpeed(currentPieceIndex + 2)) * 3;
//                float speedCommand3 = currentSpeed - (currentSpeed - getOptimizedSpeed(currentPieceIndex + 3)) * 1;
//                float ppc = speedCommand1 < speedCommand2 ? speedCommand1 : speedCommand2;
//                speedCommand = ppc < speedCommand3 ? ppc : speedCommand3;
//            } else {
//                            if (force1 == 0) {
//                speedCommand = 1;
//                            } else {
                // TODO : replace UPDATE_SIZE with optimized
//                                speedCommand = speed + UPDATE_SIZE / 2;
//                                speedCommand = 1;
//                            }
//            }
        }
        if (speedCommand > 1) {
            speedCommand = 1;
        }
        if (speedCommand < 0) {
            speedCommand = 0f;
        }
//        System.out.print(" \tK : " + utils.formatNumber(K));// + " \tforce : " + utils.formatNumber(force1) + ";" + utils.formatNumber(force2) + ";" + utils.formatNumber(force3));
//        System.out.print(" \tCommand : " + utils.formatNumber(speedCommand) + '\n');
        previousCarPosition = myCarPosition;

        return speedCommand;
    }

    // distance nécessaire pour passer de la vitesse v1 à la vitesse v2
    // TODO optim serait bien par-ce que ça dépend de l'accélération précédente aussi par exemple
    public double distanceRalentissement(float v1, float v2) {
        return ((distanceArret(v1) - distanceArret(v2))) + v1 * 10;
    }

    public float distanceArret(float v1) {
        return -v1 * v1 / (2*deceleration);
    }


    private float getMinSpeed(List<Integer> nextBendPieces) {
        float nextMinCommand=1;
        for (Integer nextBendPiece : nextBendPieces) {
            float optimizedSpeed = (float) getOptimizedSpeed(nextBendPiece);
            if (optimizedSpeed < nextMinCommand) {
                nextMinCommand = optimizedSpeed;
            }
        }
        return nextMinCommand;
    }

    private float getDistance(CarPosition myCarPosition, Integer position) {
        int currentPieceIndex = myCarPosition.piecePosition.pieceIndex;
        if (currentPieceIndex == position) {
            return 0;
        }
        List<Track.Piece> pieces = race.track.pieces;
        float distance = 0;
        for (int i = 0; i < pieces.size(); i++) {
            if (i + currentPieceIndex == position) {
                return distance;
            }
            // FIXME bend;
            Track.Piece piece = pieces.get((i + currentPieceIndex)%race.track.pieces.size());
            if (i == 0) {
//                if (!piece.isBend()) {
                    double lengthRestante = piece.getDistance(0) - myCarPosition.piecePosition.inPieceDistance;
                    if (lengthRestante > 0 && lengthRestante < piece.getDistance(0)) {
                        distance += lengthRestante;
                    }
//                } else {
//                    distance += piece.getDistance(0);
//                }
            } else {
                distance += piece.getDistance(0);
            }
        }
        return distance;

    }

    private List<Integer> getNextBendPieces(int currentPieceIndex) {
        List<Track.Piece> pieces = race.track.pieces;
        List<Integer> bendPieces = Lists.newLinkedList();
        Boolean bendFound = false;
        Boolean endOfBend = false;
        float angle= Float.MAX_VALUE;
        for (int i = 0; i < pieces.size() && !endOfBend; i++) {
            int indexToUse = (i + currentPieceIndex) % race.track.pieces.size();
            Track.Piece piece = pieces.get(indexToUse);

            if (piece.isBend() && angle != Float.MAX_VALUE) {
                if (angle == 0 /*|| (angle > 0 && piece.angle > 0) || (angle < 0 && piece.angle < 0)*/) {
                    bendPieces.add(indexToUse);
                    bendFound = true;
                }
            }
            if (!piece.isBend()/* || (angle > 0 && piece.angle < 0) || (angle < 0 && piece.angle > 0))*/&& bendFound) {
                endOfBend = true;
            }
            angle = piece.angle;
        }
        return bendPieces;

    }

    private float getForce(int pieceIndex) {

        Track.Piece piece = race.track.pieces.get((pieceIndex) % race.nbPieces);
        return piece.radius != 0 ? masse * currentSpeed * currentSpeed / piece.radius : 0;
    }

    private float getOptimizedSpeed(int pieceIndex) {
        Track.Piece piece = race.track.pieces.get((pieceIndex) % race.nbPieces);
        if (!piece.isBend()) {
            return 1;
        }
        return (float) Math.sqrt(K * Math.abs(piece.radius) / masse);
    }

    public float getCurrentSpeed(CarPosition myCarPosition, float previousSpeed) {
        float currentSpeed;
        if (race == null || previousCarPosition == null) {
            currentSpeed = 0;
        } else {
            int currentPieceIndex = myCarPosition.piecePosition.pieceIndex;
            int previousPieceIndex = previousCarPosition.piecePosition.pieceIndex;
            float currentPieceDistance = myCarPosition.piecePosition.inPieceDistance;

            //Les calculs sont différents si on a changé de pièce ou pas
            if (currentPieceIndex == previousPieceIndex) {
                float previousPieceDistance = previousCarPosition.piecePosition.inPieceDistance;
                currentSpeed = (currentPieceDistance - previousPieceDistance) / 10f;
            } else {
                Track.Piece previousPiece = race.track.pieces.get(previousCarPosition.piecePosition.pieceIndex);
                Track.Lane previousLane = race.track.lanes.get(previousCarPosition.piecePosition.lane.startLaneIndex);

                //Il y a un bug, la formule marche pas du tout quand la pièce d'avant est un switch !!! Pour pas tomber dedans, on actualise pas la vitesse
                // FIXME : on peut estimer suivant la commande précédente
                if (!previousPiece.isSwitch) {
                    float previousPieceDistance = previousPiece.getDistance(previousLane.distanceFromCenter) - previousCarPosition.piecePosition.inPieceDistance;
                    currentSpeed = (currentPieceDistance + previousPieceDistance) / 10f;
                } else {
                    currentSpeed = previousSpeed;
                }
            }
        }
        previousCarPosition = myCarPosition;
        return currentSpeed;
    }

    public void reset(Race race) {
        previousCarPosition = null;
        currentSpeed = 0f;
    }
}
