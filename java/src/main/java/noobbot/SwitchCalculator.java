package noobbot;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import noobbot.messages.Switch;
import noobbot.models.CarPosition;
import noobbot.models.Race;
import noobbot.models.Track;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * User: vmaubert
 * Date: 16/04/14
 * Time: 16:27
 */
public class SwitchCalculator {


    int indexPieceLastSwitch = -1;
    final SwitchCalculator.Path bestPath;
    final Race race;

    public SwitchCalculator(Race race) {
        this.race = race;
        bestPath = calculateBestPath(race);
    }

    public Path calculateBestPath(Race race) {
        //on veut définir le chemin le plus court pour chaque lane
        List<Path> paths = Lists.newArrayList();

        //On va faire un premier filtrage pour prendre que les virages et les pièces avec un switch.
        //On ignore les autres car elles n'apportent aucune différence entre les différents chemins
        List<Track.Piece> interestingPieces = new ArrayList<Track.Piece>();
        for (Track.Piece piece : race.track.pieces) {
            if (piece.isBend() || piece.isSwitch) {
                interestingPieces.add(piece);
            }
        }

        //Pour les calculs, il faut que le première pièce intéressante soit un switch.
        if (!interestingPieces.get(0).isSwitch) {
            List<Track.Piece> newInterestingPieces = Lists.newArrayList();
            int firstSwitchIndex = 0;
            for (Track.Piece interestingPiece : interestingPieces) {
                if (interestingPiece.isSwitch) {
                    firstSwitchIndex = interestingPieces.indexOf(interestingPiece);
                    break;
                }
            }
            newInterestingPieces.addAll(interestingPieces.subList(firstSwitchIndex, interestingPieces.size()));
            newInterestingPieces.addAll(interestingPieces.subList(0, firstSwitchIndex));
            interestingPieces = newInterestingPieces;
        }


        //TODO A optimiser
        Path bestPath = null;
        for (Track.Lane lane : race.track.lanes) {
            Path path = new Path();
            path = calculatePath(interestingPieces, lane, path, 0, race.track.lanes);
            bestPath = getMinPath(path, bestPath);
        }

        return bestPath;
    }

    private Path calculatePath(List<Track.Piece> interestingPieces, Track.Lane currentLane, Path path, int i, List<Track.Lane> lanes) {
        if (interestingPieces.size() == i) {
            return path;
        }

        Track.Piece piece = interestingPieces.get(i);

        //Si c'est un virage on calcule la longeur à ajouter au path
        if (piece.isBend()) {
            //TODO à vérifier mais je pense qu'il faut tenir compte de la vitesse max
            //apparemment sur france, il ne faut pas prendre le 2ème switch
            path.lenght += piece.getDistance(currentLane.distanceFromCenter);
        }


        Path leftPath = null;
        Path rightPath = null;
        //Si on peut switcher sur cette pièce
        if (piece.isSwitch) {

            if (currentLane.index != 0) {
                //On fait un nouveau path qui part vers la gauche
                Track.Lane leftLane = lanes.get(currentLane.index - 1);
                leftPath = new Path(path);
                leftPath.path.put(piece.index, leftLane.index);
                leftPath = calculatePath(interestingPieces, leftLane, leftPath, i + 1, lanes);
            }

            if (currentLane.index != lanes.size() - 1) {
                //On fait un nouveau path qui part vers la droite
                Track.Lane rightLane = lanes.get(currentLane.index + 1);
                rightPath = new Path(path);
                rightPath.path.put(piece.index, rightLane.index);
                rightPath = calculatePath(interestingPieces, rightLane, rightPath, i + 1, lanes);
            }

            //on a pas tourné sur ce switch pour le path courant
            path.path.put(piece.index, currentLane.index);
        }

        //Et on continue à calculer notre path courant qui va tout droit
        path = calculatePath(interestingPieces, currentLane, path, i + 1, lanes);

        return getMinPath(path, getMinPath(rightPath, leftPath));
    }

    private Path getMinPath(Path path1, Path path2) {
        if (path1 == null) {
            return path2;
        }

        if (path2 == null || path2.lenght >= path1.lenght) {
            return path1;
        }

        return path2;
    }

    public Switch.Side getSwitchCommand(CarPosition myCarPosition, int currentPieceIndex, int bumpCount, Boolean qualif) {

        Switch.Side switchCommand = null;
        boolean isSwitch = race.track.pieces.get((currentPieceIndex + 1) % race.nbPieces).isSwitch;
        if (isSwitch && indexPieceLastSwitch != currentPieceIndex) {

            int currentLaneIndex = myCarPosition.piecePosition.lane.startLaneIndex;
            int bestLaneIndex = bestPath.path.get(currentPieceIndex + 1);
            if (currentLaneIndex == bestLaneIndex) {
                switchCommand = Switch.Side.NOTHING;
            } else if (currentLaneIndex < bestLaneIndex) {
                switchCommand = Switch.Side.RIGHT;
            } else {
                switchCommand = Switch.Side.LEFT;
            }
            indexPieceLastSwitch = currentPieceIndex;

            if (bumpCount > 0 && switchCommand.equals(Switch.Side.NOTHING)) {
                List<Track.Lane> lanes = race.track.lanes;
                if (currentLaneIndex == lanes.size() - 1) {
                    switchCommand = (Math.random() < 0.75) ? Switch.Side.LEFT : Switch.Side.NOTHING;
                } else if (currentLaneIndex == 0) {
                    switchCommand = (Math.random() < 0.75) ? Switch.Side.RIGHT : Switch.Side.NOTHING;
                } else {
                    switchCommand = (Math.random() < 0.5) ? Switch.Side.LEFT : Switch.Side.RIGHT;
                }
            }
        }

        return switchCommand;
    }

    public void reset() {
        indexPieceLastSwitch = -1;
    }

    //Classe qui permet de stocker le chemin le plus court quand on démarre d'une lane
    public class Path {

        //Comportement à effectuer pour chaque switch
        public Map<Integer, Integer> path = Maps.newHashMap();
        //Longueur du path, attention pour gagner du temps, on ignore complement les lignes droites
        public double lenght = 0d;

        public Path() {

        }

        public Path(Path path) {
            this.path = Maps.newHashMap(path.path);
            this.lenght = path.lenght;
        }
    }
}
