package noobbot;

import com.google.common.collect.Lists;
import noobbot.models.Race;
import noobbot.models.Track;

import java.util.List;

/**
 * User: vmaubert
 * Date: 19/04/14
 * Time: 12:41
 */
public class TurboCalculator {

    private final Race race;
    public Integer indexStraight = null;

    public TurboCalculator(Race race) {
        this.race = race;
        int nbPieces = 0;
        int tempIndex = -1;
        int tempNbPieces = 0;

        //TODO pour les qualifs, mieux faire 2 turbos sur le même tour, que 2 sur 2 tours différents

        //TODO en qualif, ne pas utiliser le turbo que le premier tour
        //Pour les calculs il faut que la première pièce soit un virage
        List<Track.Piece> pieces = Lists.newArrayList();
        if (race.track.pieces.get(0).isBend()) {
            pieces = race.track.pieces;
        } else {
            int firstBendIndex = 0;
            for (Track.Piece piece : race.track.pieces) {
                if (piece.isBend()) {
                    firstBendIndex = piece.index;
                    break;
                }
            }
            pieces.addAll(race.track.pieces.subList(firstBendIndex, race.track.pieces.size()));
            pieces.addAll(race.track.pieces.subList(0, firstBendIndex));
        }

        //On recherche l'index de la pièce qui comme la plus grande ligne droite du circuit
        for (Track.Piece piece : pieces) {
            if (tempIndex == -1 && !piece.isBend()) {
                tempIndex = piece.index;
            }

            if (!piece.isBend()) {
                tempNbPieces++;
            } else if (tempIndex != -1) {
                if (tempNbPieces > nbPieces) {
                    nbPieces = tempNbPieces;
                    indexStraight = tempIndex;
                }
                tempIndex = -1;
                tempNbPieces = 0;
            }
        }

        if (tempNbPieces > nbPieces) {
            indexStraight = tempIndex;
        }

        //TODO faut que la ligne droite soit d'une longueur minimale, car on bloque le turbo dans le main

    }

    public boolean getTurboCommand(int currentPieceIndex) {

        if (indexStraight != null && currentPieceIndex == indexStraight) {
//            System.out.println("TURBOOOOOOOOOOOOOOOOOOOOOOO currentPieceIndex = " + currentPieceIndex);
            return true;
        }

        return false;
    }
}
