package noobbot;

/**
 * Created by mattboll on 19/04/14.
 */
public class Utils {

    public String formatNumber(double number) {
        return String.format("%.2f", number);
    }
}
