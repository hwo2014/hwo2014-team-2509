package noobbot;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import noobbot.models.CarPosition;
import noobbot.models.Race;
import noobbot.models.Track;
import noobbot.models.TurboAvailable;

import java.util.List;
import java.util.Map;

/**
 * User: vmaubert
 * Date: 19/04/14
 * Time: 12:41
 */
public class VMSpeedCalculator {

    private CarPosition previousCarPosition = null;
    protected Race race;
    float speedCommand = 1f;
    Map<Integer, List<Float>> maxSpeedByPiece = Maps.newHashMap();
    List<Track.Piece> interestingBendList = Lists.newArrayList();
    private Track.Piece nextBend = null;
    private boolean noMoreBend = false;
    private float distanceToNextBend;

    private float breakCoeff = -0.0034f;
    private boolean breakCoeffCalculated = false;
    private float bendCoeff = 0.0045f;
    private boolean bendCoeffCalculated = false;
    private int breakPieceIndex = -1;
    private float speed1 = 0;
    private float dist1 = 0;
    private int nbBreakPointSaved = 0;

    public VMSpeedCalculator(Race race) {
        this.race = race;
        initData(race);
    }

    private void initData(Race race) {
        for (int i = 0; i < race.nbPieces; i++) {
            Track.Piece piece = race.track.pieces.get(i);
            List<Float> maxSpeedList = Lists.newArrayList();
            for (Track.Lane lane : race.track.lanes) {
                maxSpeedList.add(getMaxSpeed(piece, lane.distanceFromCenter));
            }
            maxSpeedByPiece.put(i, maxSpeedList);
        }

        List<Track.Piece> bendList = Lists.newArrayList();
        Track.Piece lastPiece = race.track.pieces.get(race.nbPieces - 1);
        for (Track.Piece piece : race.track.pieces) {
            if (piece.isBend()) {
                if (bendList.isEmpty()) {
                    bendList.add(piece);
                } else if (!lastPiece.isBend() || lastPiece.radius > piece.radius) {
                    bendList.add(piece);
                }
            }
            lastPiece = piece;
        }

        for (Track.Piece currentBend : bendList) {

            if (interestingBendList.isEmpty()) {
                interestingBendList.add(currentBend);
            } else {
                Track.Piece lastBend = interestingBendList.get(interestingBendList.size() - 1);
                //TODO attention ça marche pas si on est sur deux lanes différentes
                //On calcule la distance entre la début du premier virage et le deuxième.
                float distance = getDistanceBetween2Pieces(lastBend.index, 0, currentBend.index);

                //TODO marche qu'avec 2 lanes
                float lastBendMaxSpeed = Math.max(maxSpeedByPiece.get(lastBend.index).get(0), maxSpeedByPiece.get(lastBend.index).get(1));
                float currentBendMinSpeed = Math.min(maxSpeedByPiece.get(currentBend.index).get(0), maxSpeedByPiece.get(currentBend.index).get(1));

                //On calcule la distance pour freiner si on sort du dernier virage en vitesse max
                float breakDistance = getBreakDistanceBetween2Speed(lastBendMaxSpeed, currentBendMinSpeed);


                //Cette distance est-elle suffisante pour freiner ?
                if (breakDistance > distance) {
                    //Si ce n'est pas le cas on ignore le virage précédent,
                    //car il faut freiner avant le premier virage pour ne pas sortir dans le 2ème
                    interestingBendList.remove(lastBend);
                }
                interestingBendList.add(currentBend);

                //TODO faut vérifier qu'il ne faut pas freiner avec le(s) dernier(s) virage(s) pour ne pas arriver trop vite dans le premier virage du circuit
            }
        }
    }

    public float getSpeedCommand(CarPosition myCarPosition, int currentPieceIndex, float currentSpeed) {

        Track.Lane currentLane = race.track.lanes.get(myCarPosition.piecePosition.lane.endLaneIndex);
        Track.Piece currentPiece = race.track.pieces.get(currentPieceIndex);
        speedCommand = getMaxSpeed(currentPiece, currentLane.distanceFromCenter);

//        System.out.print("\nmaxSpeed = " + speedCommand);
//        System.out.print("\tangle = " + myCarPosition.angle);
        nextBend = getNextBend(currentPieceIndex, myCarPosition.piecePosition.lap);
//        System.out.print("\tspeed = " + currentSpeed);

        if (noMoreBend) {
            if (!currentPiece.isBend()) {
                speedCommand = 1f;
            }
        } else {
            //On calcule la distance a effectuer pour atteindre le prochain virage.
            distanceToNextBend = getDistanceBetween2Pieces(currentPieceIndex, myCarPosition.piecePosition.inPieceDistance, nextBend.index);

            //On calcule la distance de freinage pour arriver dans ce virage à la vitesse max
            float breakDistance = getBreakDistanceBetween2Speed(currentSpeed, maxSpeedByPiece.get(nextBend.index).get(currentLane.index));
//            System.out.print("\tdistanceToNextBend = " + distanceToNextBend);
//            System.out.print("\tbreakDistance = " + breakDistance);

            //Si cette distance n'est pas suffisante, on freine
            if (breakDistance > distanceToNextBend) {
                speedCommand = 0f;
            }
        }


        if (speedCommand > 1) {
            speedCommand = 1;
        }
        if (speedCommand < 0) {
            speedCommand = 0f;
        }


        float realAngle = Math.abs(myCarPosition.angle);
        if (speedCommand != 1) {
            float error = speedCommand - currentSpeed;
            if (error > 0.01) {
                if (error > 0.1) {
                    speedCommand = 1;
                } else {
                    // plus y a d'angle et plus la voiture ralentie toute seule
                    speedCommand += error * (1 + realAngle / 45);
                }
            } else if (error < -0.0001) {
                // quand on est trop rapide, on fait en sorte qu'au prochain tick on soit à la bonne vitesse (plus ou moins)
                if (error < -0.02) {
                    speedCommand = 0;
                } else if (error < -0.01) {
                    speedCommand += error * 10;
                } else {
                    speedCommand += error * 3;
                }
                //Si on ne connait pas encore la constante de freinage et qu'on freine en ligne droite,
                // on essaye de la calculer. On ne fait aucun calcul sur un virage ou un switch, car c'est pas du tout précis.
                if (!breakCoeffCalculated && !currentPiece.isBend() && !currentPiece.isSwitch) {
                    if (breakPieceIndex != currentPieceIndex) {
                        nbBreakPointSaved = 0;
                        breakPieceIndex = currentPieceIndex;
                    }
                    if (nbBreakPointSaved == 1) {
                        speed1 = currentSpeed;
                        dist1 = myCarPosition.piecePosition.inPieceDistance;
                    } else if (nbBreakPointSaved == 9) {
                        breakCoeffCalculated = true;
                        float speed2 = currentSpeed;
                        float dist2 = myCarPosition.piecePosition.inPieceDistance;
                        breakCoeff = (speed2 * speed2 - speed1 * speed1) / (dist2 - dist1);
                    }

                    nbBreakPointSaved++;
                }
            } else {
                //Dans le cas où on est à l'équilibre, on peut vérifier si l'angle est bon ou non
                if (realAngle < 52 && !bendCoeffCalculated) {
                    bendCoeff += 0.000002;
                }
            }
        }
        if (realAngle > 52) {
            bendCoeffCalculated = true;
            if (realAngle > 55) {
                bendCoeff -= 0.000004;
            }
        }

//            System.out.println("bendCoeff = " + bendCoeff);
//        System.out.print("\tspeedCommand = " + speedCommand);
//        System.out.println("breakCOeff = " + breakCoeff);
        return speedCommand;
    }

    /**
     * @param currentPieceIndex
     * @param currentLap
     * @return le prochain virage où l'on doit freiner pour le prendre
     */
    public Track.Piece getNextBend(int currentPieceIndex, int currentLap) {
        if (!noMoreBend) {
            nextBend = interestingBendList.get(0);
            boolean previousBendFound = false;
            for (Track.Piece bend : interestingBendList) {
                if (bend.index > currentPieceIndex) {
                    if (previousBendFound) {
                        nextBend = bend;
                    }
                    break;
                } else {
                    previousBendFound = true;
                }
            }

            //Attention en qualif le raceSession.laps == 0
            //TODO optimiser en qualif, si je n'ai pas le temps de faire un tour supplémentaire,
            // on doit foncer le plus vite possible vers la ligne d'arrivée sans freiner
            if (race.raceSession.laps != 0 && race.raceSession.laps == currentLap + 1 && nextBend.index <= currentPieceIndex) {
                //Si c'est la fin dernier tour, il n'y a pas d'autre virage
                noMoreBend = true;
            }
        }

        return nextBend;
    }

    private Float getMaxSpeed(Track.Piece piece, int distanceFromCenter) {
        Float maxSpeed;
        if (!piece.isBend()) {
            maxSpeed = new Float(1);
        } else {
            //En fonction du sens du virage, il faut soit ajouter sous soustraire distanceFromCenter du virage
            int sens = -1;
            if (piece.angle < 0) {
                sens = 1;
            }
            float rayon = piece.radius + sens * distanceFromCenter;
            maxSpeed = new Float(Math.sqrt(rayon * bendCoeff));

        }

        return maxSpeed;
    }

    public float getCurrentSpeed(CarPosition myCarPosition, float previousSpeed, boolean turboLaunched, TurboAvailable turbo) {
        float currentSpeed;
        if (race == null || previousCarPosition == null) {
            currentSpeed = 0;
        } else {
            int currentPieceIndex = myCarPosition.piecePosition.pieceIndex;
            int previousPieceIndex = previousCarPosition.piecePosition.pieceIndex;
            float currentPieceDistance = myCarPosition.piecePosition.inPieceDistance;

            //Les calculs sont différents si on a changé de pièce ou pas
            if (currentPieceIndex == previousPieceIndex) {
                float previousPieceDistance = previousCarPosition.piecePosition.inPieceDistance;
                currentSpeed = (currentPieceDistance - previousPieceDistance) / 10f;
            } else {
                Track.Piece previousPiece = race.track.pieces.get(previousCarPosition.piecePosition.pieceIndex);
                Track.Lane previousLane = race.track.lanes.get(previousCarPosition.piecePosition.lane.startLaneIndex);

                //Il y a un bug, la formule marche pas du tout quand la pièce d'avant est un switch !!! Pour pas tomber dedans, on actualise pas la vitesse
                if (!previousPiece.isSwitch) {
                    float previousPieceDistance = previousPiece.getDistance(previousLane.distanceFromCenter) - previousCarPosition.piecePosition.inPieceDistance;
                    currentSpeed = (currentPieceDistance + previousPieceDistance) / 10f;
                } else {
                    currentSpeed = previousSpeed;
                }
            }
        }
        previousCarPosition = myCarPosition;
        return currentSpeed;
    }

    /**
     * @param indexPiece1
     * @param distanceOnPiece1AlreadyDone
     * @param indexPiece2
     * @return La distance entre le début de la première pièce et le début de la deuxième
     */
    public float getDistanceBetween2Pieces(int indexPiece1, float distanceOnPiece1AlreadyDone, int indexPiece2) {
        if (indexPiece1 == indexPiece2) {
            return 0;
        }
        float distance = 0f;
        List<Track.Piece> path;
        if (indexPiece1 < indexPiece2) {
            path = race.track.pieces.subList(indexPiece1, indexPiece2);
        } else {
            path = Lists.newArrayList();
            for (int i = indexPiece1; i < race.nbPieces; i++) {
                path.add(race.track.pieces.get(i));
            }
            for (int i = 0; i < indexPiece2 - 1; i++) {
                path.add(race.track.pieces.get(i));
            }
        }
        for (Track.Piece piece : path) {
            float distanceAlreadyDone = 0f;
            if (piece.index == indexPiece1) {
                distanceAlreadyDone = distanceOnPiece1AlreadyDone;
            }
            distance += piece.getDistance(0) - distanceAlreadyDone;
        }
        return distance;
    }

    private float getBreakDistanceBetween2Speed(Float speed1, Float speed2) {
        if (speed1 < speed2) {
            return 0;
        }

        return (((speed2 * speed2) - (speed1 * speed1)) / breakCoeff) + speed1 * 10;
    }

    public void reset(Race race) {
        this.race = race;
        previousCarPosition = null;
        nextBend = interestingBendList.get(0);
        noMoreBend = false;
    }
}
