package noobbot.messages;

/**
 * User: vmaubert
 * Date: 15/04/14
 * Time: 16:51
 */
public class Join extends SendMsg {
    public final String name;
    public final String key;

    public Join(final String name, final String key) {
        super(0);
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }

}
