package noobbot.messages;

public class JoinRace extends SendMsg {
    public final Join botId;
    public final String trackName;
    public final int carCount;
    public final String password;

    public JoinRace(String name, String key, final String trackname, final String carCount, final String password) {
        super(0);
        this.botId = new Join(name, key);
        this.trackName = trackname;
        if (carCount != null && !carCount.equals("")) {
            this.carCount = Integer.valueOf(carCount);
        } else {
            this.carCount = 1;
        }
        this.password = password;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}
