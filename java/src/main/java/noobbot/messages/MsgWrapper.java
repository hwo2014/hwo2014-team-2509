package noobbot.messages;

/**
 * User: vmaubert
 * Date: 15/04/14
 * Time: 16:51
 */
public class MsgWrapper {
    public final String msgType;
    public final Object data;
    public final int gameTick;

    MsgWrapper(final String msgType, final Object data, int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData(), sendMsg.gameTick());
    }
}
