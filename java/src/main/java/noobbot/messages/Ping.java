package noobbot.messages;

/**
 * User: vmaubert
 * Date: 15/04/14
 * Time: 16:50
 */
public class Ping extends SendMsg {


    public Ping(int gameTick) {
        super(gameTick);
    }

    @Override
    protected String msgType() {
        return "ping";
    }
}