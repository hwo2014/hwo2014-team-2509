package noobbot.messages;

/**
 * User: vmaubert
 * Date: 15/04/14
 * Time: 16:51
 */
public class Switch extends SendMsg {
    private Side side;

    public Switch(Side side, int gameTick) {
        super(gameTick);
        this.side = side;
    }

    @Override
    protected Object msgData() {
        return side.value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }

    public enum Side {
        LEFT("Left"),
        RIGHT("Right"),
        NOTHING(null);

        public final String value;

        Side(String value) {
            this.value = value;
        }
    }

}
