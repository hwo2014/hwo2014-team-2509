package noobbot.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class CarPositionData {

    @SerializedName("data")
    public List<CarPosition> carPositions;

    public String gameId;
    public int gameTick;
}
