package noobbot.models;

/**
 * User: vmaubert
 * Date: 24/04/14
 * Time: 16:29
 */
public class TurboAvailable {

    public Data data;

    public class Data {
        public int turboDurationTicks;
        public int turboFactor;
    }
}
